$(document).ready(function () {

    function fetchData(arr, index) {
        var csv = [];
        for(var i=0;i<arr.length;i++){
            csv.push(arr[i][index]);
        }
        return csv;
    }

    $("#migration_table").DataTable(
        {
            ordering: true,
            order: [[ 1, "desc" ]]
        }
    );

    var table = $('#files_table').DataTable({
        dom: "Bfrtip",
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        buttons: [
            {
                text: 'Delete',
                className: 'btn btn-danger',
                action: function () {
                    if (table.rows({selected: true}).data() != null) {
                        var filenames = fetchData(table.rows({selected:true}).data(),1).join();
                        bootbox.confirm({
                            title: "Delete",
                            message: "Are you sure want to delete?",
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> Cancel'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Delete'
                                }
                            },
                            callback: function (result) {
                                if(result == true) {
                                    $.post("/main/delete_file", {filenames: filenames}, function(data){
                                        if(data==0) {
                                            toastr.success("Data telah dihapus");
                                            var rows = table.draw();
                                        } else {
                                            toastr.error("Error when deleting data. Code Error: " + data);
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        bootbox.alert("You have not selected any data!");
                    }
                }


            }
        ]
    });

    if(document.getElementById("migration_result_save")){
        document.getElementById("migration_result_save").onclick = function () {
            // when clicked the button
            var content = document.getElementById('migration_result').value;
            // a [save as] dialog will be shown
            window.open("data:application/txt," + encodeURIComponent(content), "_self");
        }
    }
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}