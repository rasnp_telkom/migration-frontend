<?php

/**
 * Created by PhpStorm.
 * User: mfikria
 * Date: 6/2/2017
 * Time: 10:33 AM
 */
class File_model extends CI_Model
{
    private $client;
    public function __construct()
    {
        parent::__construct();

        $this->client = new GuzzleHttp\Client();
    }

    public function list_impu($data) {
        $response = $this->client->request(
            'POST',
            'http://36.84.100.162/migration-backend/user_migration/get_all_impu',
            [
                'form_params' => [
                    'impu' => json_encode($data)
                ]
            ]
        );

        $response_array = json_decode($response->getBody()->getContents(), TRUE);

        return $response_array['data'];
    }

    public function list_file() {
        $response = $this->client->request(
            'POST',
            'http://36.84.100.162/migration-backend/user_migration/get_all_file'
        );

        $response_array = json_decode($response->getBody()->getContents(), TRUE);

        return $response_array['data'];
    }

    public function delete_file($filenames){

        $response = $this->client->request(
            'POST',
            'http://36.84.100.162/migration-backend/user_migration/delete_file',
            [
                'form_params' => [
                    'filenames' => $filenames
                ]
            ]
        );

        $response_array = json_decode($response->getBody()->getContents(), TRUE);

        return $response_array['data'];
    }


}
