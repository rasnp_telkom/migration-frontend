<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('create_symbol'))
{
    function create_symbol($param)
    {
        if ($param == 1) {
            return "<i style='color: green' class='fa fa-check fa-lg' aria-hidden></i>";
        } else if($param == 0){
            return "<i style='color: red' class='fa fa-times fa-lg' aria-hidden></i>";
        } else {
            return "<i style='color: orange' class='fa fa-question fa-lg' aria-hidden></i>";
        }

    }
}

function set_toast_notification($type, $message) {
    $toastr = array(
        'type' => $type,
        'message' => $message
    );

    return $toastr;
}