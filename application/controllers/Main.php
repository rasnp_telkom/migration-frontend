<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('main');

        $this->load->model('migration_model');
        $this->load->model('file_model');

        $this->load->library("session");

    }

    public function index()
    {
        $this->load->view('main');
    }


    public function process_file()
    {
        if(!empty($_FILES['userfile']['name'])) {
            $filename = str_replace(" ", "_", $_FILES['userfile']['name']);
            $tmp_name = $_FILES['userfile']['tmp_name'];

            $response_upload = $this->migration_model->upload($filename, $tmp_name);
            if ($response_upload['code'] == 0) {


                $data['has_uploaded'] = TRUE;
                $response_migrate = $this->migration_model->migrate($filename);
                if ($response_migrate['code'] == 0) {
                    $data['data_migration'] = $response_migrate['data'];
                    $this->session->set_flashdata("toastr", set_toast_notification('success', "Upload dan migrasi telah berhasil!"));
                } else {
                    $this->session->set_flashdata("toastr", set_toast_notification('error', "Terjadi kesalahan pada migrasi. Silakan cek dan upload kembali."));
                }
            } else {

            }
        } else {
            $this->session->set_flashdata("toastr", set_toast_notification('error', "File belum terpilih. Silakan pilih file terlebih dahulu"));
        }

        $data['impus'] = $this->file_model->list_impu($response_migrate['data']['impu']);
        $data['files'] = $this->file_model->list_file();
        $this->load->view('main', $data);
    }

    public function delete_file()
    {
        $filenames = $this->input->post('filenames');
        $return = $this->file_model->delete_file($filenames);
    }
}
