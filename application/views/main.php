<?php
$fitur_parameters = array(
    "abbreviated",
    "call_waiting",
    "conference",
    "cfb",
    "cfu",
    "cfnr",
    "clip",
    "clir",
    "hotline_instant",
    "keyword",
    "migrated"
);


?>
<!DOCTYPE html>

<? //=var_dump($fitur_impu)?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Portal Simulasi Migrasi dari IMS Ericsson Operasional Surabaya dengan Menggunakan Database HSS (Offline) ke
        IMS Huawei Lab. DDS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url('/assets/plugins/datatables/dataTables.bootstrap.css') ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('/assets/css/app.css') ?>">
    <link rel="stylesheet" href="<?= base_url('/assets/css/custom.css') ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url('/assets/css/_all-skins.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-collapse">
<div class="wrapper">

    <header class="main-header">

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <!--            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">-->
            <!--                <span class="sr-only">Toggle navigation</span>-->
            <!--                <span class="icon-bar"></span>-->
            <!--                <span class="icon-bar"></span>-->
            <!--                <span class="icon-bar"></span>-->
            <!--            </a>-->
            <!-- Logo -->
            <a href="<?= base_url() ?>" class="logo" style="width: 100%">
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Portal Simulasi Migrasi</b></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                </ul>
            </div>
        </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container">
            <section class="content-header">
                <div class="callout callout-default">
                    <h4>Selamat Datang!</h4>
                    <p>Portal Simulasi Migrasi dari IMS Ericsson Operasional Surabaya dengan Menggunakan Database HSS
                        (Offline) ke IMS Huawei Laboratorium DDS, PT Telkom Indonesia.</p>
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body">
                                <?php echo form_open_multipart('main/process_file'); ?>
                                <div class="box-body">
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-2">
                                        <div class="col-xs-12 col-sm-3">
                                            <label for="exampleInputFile">File input</label>
                                        </div>
                                        <div class="col-xs-12 col-sm-9">

                                            <input type="file" name="userfile"/>

                                            <p class="help-block">Upload file configuration here.</p>

                                        </div>


                                    </div>

                                    <div class="col-xs-12 col-sm-4">
                                        <button type="submit" value="upload" class="btn btn-block btn-primary">Upload &
                                            Migrate
                                        </button>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                </form>
                            </div>
                        </div>
                        <?php if (!isset($has_uploaded)): ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                </button>
                                <h4><i class="icon fa fa-info"></i> Info!</h4>
                                Please upload your file configuration to migrate the data.
                            </div>
                        <?php else: ?>
                            <div class="box">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Migration Result</label>
                                        <textarea class="form-control" id="migration_result" rows="3"
                                                  disabled="">
                                                <?= $data_migration['commands']?>
                                            </textarea>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <a type="button" id="migration_result_save" title="Save as text file"
                                       class="btn btn-primary btn-block">Download</a>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if (isset($has_uploaded)): ?>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">User Features</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="migration_table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Impu</th>
                                        <th>Abbreviated</th>
                                        <th>Call Waiting</th>
                                        <th>Conference</th>
                                        <th>CFB</th>
                                        <th>CFU</th>
                                        <th>CFNR</th>
                                        <th>CLIP</th>
                                        <th>CLIR</th>
                                        <th>Hotline Instant</th>
                                        <th>Keyword</th>
                                        <th>Migrated</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($impus as $impu): ?>
                                        <tr>
                                            <td><?= $impu['impu'] ?></td>
                                            <td>
                                                <?= create_symbol($impu['abbreviated']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['call_waiting']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['conference']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['cfb']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['cfu']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['cfnr']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['clip']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['clir']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['hotline_instant']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['keyword']) ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($impu['migrated']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Files Uploaded</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="files_table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Filename</th>
                                        <th>Timestamp</th>
                                        <th>Migrated</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($files as $file): ?>
                                        <tr>
                                            <td></td>
                                            <td><?= $file['filename'] ?></td>
                                            <td>
                                                <?= $file['timestamp'] ?>
                                            </td>
                                            <td>
                                                <?= create_symbol($file['migrated']) ?>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <?php endif;?>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<!-- DataTables -->
<script src="<?= base_url();?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/plugins/datatables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/plugins/datatables/extensions/buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url();?>assets/plugins/datatables/extensions/select/js/dataTables.select.min.js"></script>

<script src="<?= base_url(); ?>assets/plugins/bootbox/bootbox.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>

<!-- SlimScroll -->
<script src="<?= base_url('/assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('/assets/plugins/fastclick/fastclick.js') ?>"></script>
<script src="<?= base_url('/assets/js/app.js') ?>"></script>
<script src="<?= base_url('/assets/js/custom.js') ?>"></script>

<script type="text/javascript">
    <?php if ($this->session->flashdata('toastr') != null): ?>
    <?php $toastr = $this->session->flashdata('toastr');
    switch ($toastr['type']) :
    case 'success': ?>
    toastr.success("<?=$toastr['message']?>");
    <?php  break;
    case 'warning': ?>
    toastr.warning("<?=$toastr['message']?>");
    <?php  break;
    case 'info': ?>
    toastr.info("<?=$toastr['message']?>");
    <?php  break;
    case 'error': ?>
    toastr.error("<?=$toastr['message']?>");
    <?php  break;
    default: ?>
    toastr.error("<?=$toastr['message']?>");
    <?php break;
    endswitch;?>
    <?php endif;?>
</script>

</body>
</html>
